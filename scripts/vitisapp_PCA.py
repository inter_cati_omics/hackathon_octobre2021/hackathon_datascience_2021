# myapp.py
import pandas as pd
import sys
from sklearn.decomposition import PCA

from bokeh.layouts import column, row
from bokeh.plotting import figure, curdoc
from bokeh.models import ColumnDataSource, Select,  HoverTool
from bokeh.transform import factor_cmap
from bokeh.palettes import Category20

vitis=pd.read_pickle("../data/RPKN_counts_715_samples_3000_complete.pkl")
vitis.drop(('Gene','Vitvi03g01899'), axis=1,inplace=True)

# PCA
pca=PCA(n_components=3)
X_reduced = pca.fit_transform(vitis.loc['Sample','Gene'])
pca.explained_variance_ratio_
axis1name='axis 1 '+'('+str(round(pca.explained_variance_ratio_[0],2))+')' 
axis2name='axis 2 '+'('+str(round(pca.explained_variance_ratio_[1],2))+')'
axis3name='axis 3 '+'('+str(round(pca.explained_variance_ratio_[2],2))+')'
df = pd.DataFrame(X_reduced, columns = [axis1name,axis2name,axis3name], index=vitis.loc['Sample', 'Gene'].index) 

axismenu=[axis1name,axis2name,axis3name]
Xaxis=axis1name
Yaxis=axis2name

selmenu=['Organ', 'Species', 'Variety', 'Tissue', 'Short_stage']
sel_col_value='Organ'
color_labels=vitis.loc['Sample',('Metadata',sel_col_value)].unique()
index_cmap = factor_cmap('sel', palette=Category20[20], factors=color_labels)


def get_dataset(Xaxis, Yaxis, sel_col_value):
    sel_col=vitis.loc['Sample',('Metadata',sel_col_value)]
    cds =ColumnDataSource(data=dict(x=df.loc[:,Xaxis], y=df.loc[:,Yaxis],sel=sel_col, names=df.index))
    return cds

def make_plot(source, xaxis, yaxis):
    hover = HoverTool(
        tooltips=[
            ("lib", "@names"),
            (sel_col_value, "@sel")
        ]
    )
    p = figure(plot_width=600, plot_height=400, tools=[hover])
    circle=p.circle(source=source, x='x', y='y', fill_color=index_cmap, line_color=index_cmap, alpha=0.6)
  
    p.title.text = "PCA"
    p.xaxis.axis_label = xaxis
    p.yaxis.axis_label = yaxis
    return (p,circle)

def update_Xaxis(attrname, old, new):
    Xaxis = select1.value
    src = get_dataset(Xaxis,plot.yaxis.axis_label, select3.value)
    source.data.update(src.data)
    plot.title.text = "PCA " + Xaxis + " and " + plot.yaxis.axis_label
    plot.xaxis.axis_label = Xaxis
    

def update_Yaxis(attrname, old, new):
    Yaxis = select2.value
    src = get_dataset(plot.xaxis.axis_label,Yaxis, select3.value)
    source.data.update(src.data)
    plot.title.text = "PCA " + plot.yaxis.axis_label + " and " + Yaxis
    plot.yaxis.axis_label = Yaxis

def update_Sel(attrname, old, new):
    sel_col_value=select3.value
    src = get_dataset(plot.xaxis.axis_label,plot.yaxis.axis_label, sel_col_value)
    source.data.update(src.data)
    sel_col=vitis.loc['Sample',('Metadata',sel_col_value)]
    color_labels=vitis.loc['Sample',('Metadata',sel_col_value)].unique()
    index_cmap = factor_cmap('sel', palette=Category20[20], factors=color_labels)
    circle.glyph.fill_color=index_cmap
    circle.glyph.line_color=index_cmap
    hover = HoverTool(
        tooltips=[
            ("lib", "@names"),
            (sel_col_value, "@sel")
        ]
    )
    plot.tools=[hover]
    
source = get_dataset(Xaxis, Yaxis, sel_col_value)
(plot,circle) = make_plot(source, axis1name, axis2name)

    
select1 = Select(title="X Axis", value=axis1name, options=axismenu)
Xaxis=select1.on_change('value', update_Xaxis)

select2 = Select(title="Y Axis", value=axis2name, options=axismenu)
select2.on_change('value', update_Yaxis)

select3 = Select(title="Color", value=sel_col_value, options=selmenu)
select3.on_change('value', update_Sel)


controls = column(select1,select2, select3)

curdoc().add_root(row(plot, controls))
curdoc().title = "Vitis PCA"

