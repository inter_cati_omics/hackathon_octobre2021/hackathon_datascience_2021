# myapp.py
import pandas as pd
import sys

from bokeh.layouts import column, row
from bokeh.plotting import figure, curdoc

from bokeh.models import ColumnDataSource, Select

vitis=pd.read_pickle("../data/RPKN_counts_715_samples_3000_complete.pkl")
vitis_counts=vitis.loc[('Sample'),('Gene')]
vitis_counts.drop(('Vitvi03g01899'), axis=1,inplace=True)

menu=vitis_counts.index[:20].tolist()


def get_dataset(l1, l2):
    sys.stderr.write("Extracting data from " + l1 + " and " + l2 + "\n")
    extract_col_data = vitis_counts.loc[[l1,l2],:].transpose()
    extract_col_data.columns = ['x', 'y']
    df =ColumnDataSource(data=extract_col_data)
    return df

def make_plot(source, lib1, lib2):
    p = figure(plot_width=400, plot_height=400)
    circle=p.circle(source=source, x='x', y='y',color="firebrick", alpha=0.6)
    p.title.text = "Expression data for " + lib1 + " and " +  lib2
    p.xaxis.axis_label = lib1
    p.yaxis.axis_label = lib2
    return p

def update_plot1(attrname, old, new):
    lib1 = select1.value
    src = get_dataset(lib1,plot.yaxis.axis_label )
    plot.title.text = "Expression data for " + lib1 + " and " + lib2
    plot.xaxis.axis_label = lib1
    source.data.update(src.data)
    
def update_plot2(attrname, old, new):
    lib2 = select2.value
    plot.title.text = "Expression data for " + lib1 + " and " + lib2
    plot.yaxis.axis_label = lib2
    src = get_dataset(plot.xaxis.axis_label, lib2)
    source.data.update(src.data)
    

lib1='Car_BV'
lib2='DRR093291'
#lib1=menu[0]
#lib2=menu[1]
source = get_dataset(lib1, lib2)
plot = make_plot(source, lib1, lib2)

    
select1 = Select(title="Axis 1 library:", value=lib1, options=menu)
select1.on_change('value', update_plot1)

select2 = Select(title="Axis 2 library:", value=lib2, options=menu)
select2.on_change('value', update_plot2)

controls = column(select1,select2)

curdoc().add_root(row(plot, controls))
curdoc().title = "Vitis expression"
