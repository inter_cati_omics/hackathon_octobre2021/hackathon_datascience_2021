# Visualisation avec `ggplot2`

## Les bases 

<img src="../img/ggplot-basics.png" align="center"/> 

## TP

A partir des données préparées précédemment, en en réalisant les manipulations nécéssaires (filtres, jointures, pivot, ...), réalisez les figures suivantes :


### Barplot des différents `Organ` en fonction des `Experimentation_type`

![](../img/barplot.png)

Ce plot peut facilement être rendu dynamique avec plotly :

```{r}
# to use embed_notebook(), install pandoc in anaconda : 
# anaconda prompt :  conda install -c conda-forge pandoc
# embed_notebook() allows to view the graph entirely in the notebook
ggplotly(p) %>% embed_notebook(width="800",height="500")
```

### Nuage de point de l'échantillon `DRR093291` en fonction de `DRR093293`

![](../img/scatter.png)

### ACP des échantillon

Commencez par réaliser une ACP des échantillons avec `stat::prcomp()` (matrice de comptages avec échantillons en ligne). Les modèles créés en R peuvent facilement être "`tidyverse`ifiés" avec le package `broom` (voir [README](https://broom.tidymodels.org/) et [vignette](https://broom.tidymodels.org/articles/broom.html)). Utilisez `broom::augment()` pour ajouter les métadonnées à l'ACP, puis projetez l'ACP suivante :

![](../img/pca.png)

### Et ensuite ...

A partir des données et de la documentation, tracez divers plots pour visualiser au mieux les données !

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/R_notebooks/visualization_with_ggplot_and_plotly_R.ipynb)

## Sources

-   Cheatsheet [ggplot2](https://raw.githubusercontent.com/rstudio/cheatsheets/master/data-visualization.pdf)
