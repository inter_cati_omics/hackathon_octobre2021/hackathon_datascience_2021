# Introduction au Machine learning avec R et le package Caret

Le but de cette partie sera d'introduire l'utilisation du Machine Learning pour prédire l'organe auquel correspond un ensemble d'échantillons "tests", selon l'expression de leurs gènes grâce à un ou plusieurs modèles entrainés préalablement.

## Les packages R

Charger les packages **tidyverse**, **caret**, **e1071** et **randomForest**. S'ils ne sont pas installés, se référer à la partie [Prérequis](book/prerequis.md).

## Sources

-   Cheatsheet [caret](https://raw.githubusercontent.com/rstudio/cheatsheets/master/caret.pdf)

<img src="../img/caret.png" align="center"/> 

## Préparation de la matrice pour l'application ML

* Quand on fait du Machine Learning, les modèles à entrainer requièrent que les variables (ici les gènes) soient en colonnes et les individus/observations (ici les échantillons RNAseq) en ligne. Il faut donc faire la transposée de notre matrice précédemment filtrée (715 échantillons * 3000 gènes). Utilisez la fonction _t()_.

* Il faut aussi que la variable à prédire (organe) soit présente dans la matrice de comptage. Il faut donc ajouter cette colonne à notre matrice de comptage en faisant un merge de la matrice de comptage avec les métadonnées et en ne gardant que la colonne "Organ" du tableau des métadonnées. Fonction_ merge()_ à utiliser.

A la fin, la dimension de votre matrice doit être de 715 lignes et 3001 colonnes.

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/R_notebooks/machine_learning_introduction.ipynb)

## Création du jeu de données d'entrainement et du jeu de données test

Classiquement en ML, 80% des données sont utilisées pour entrainer le modèle et 20% sont utilisées pour le tester : voir s'il trouve la bonne réponse, ici l'organe auquel appartient chaque échantillon, en regardant l'expression de ses gènes.

* Utiliser la fonction createDataPartition() du package caret() pour sélectionner aléatoirement des indices dans le jeu de données, qui représenteront 80% du jeu de données total.

* Créer un jeu de données d'entrainement avec les indices sélectionnés (80% des données)

* Créer un jeu de données test avec l'ensemble des indices qui ne sont pas dans les indices sélectionnés précédemment (20% des données)

* Grâce à la fonction _table()_, visualiser le nombre d'échantillons / organe dans le jeu d'entrainement et le jeu de données test.

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/R_notebooks/machine_learning_introduction.ipynb)

## Entrainement d'un modèle de classification : le modèle k-NN (k Nearest Neighbors)

**Notes : **

Il est surnommé « nearest neighbors » (plus proches voisins, en français) car le principe de ce modèle consiste en effet à choisir les k données les plus proches du point étudié afin d’en prédire sa valeur.

k n'est pas un paramètre mais un hyperparamètre, c'est-à-dire que contrairement aux paramètres classiques, il ne va pas pouvoir être appris automatiquement par l'algorithme à partir des données d'entraînement. Les hyperparamètres permettent de caractériser le modèle (complexité, rapidité de convergence, etc). Ce ne sont pas les données d'apprentissage qui vont permettre de trouver ces paramètres (en l'occurrence ici, le nombre de voisins k), mais bien à nous de l'optimiser à l'aide du jeu de données test.

Source : [k-NN selon Openclassrooms](https://openclassrooms.com/fr/courses/4011851-initiez-vous-au-machine-learning/4022441-entrainez-votre-premier-k-nn)

**Le but** ici est d'entrainer le modèle k-NN afin qu'il soit capable de déterminer l'organe duquel provient l'échantillon en regardant ses k plus proches voisins.

### Choisir une méthode d'évaluation du modèle

Classiquement, pour évaluer la performance d'un modèle, on utilise une méthode de ré-échantillonnage (resampling) appelée la validation croisée à k blocs (k-fold cross-validation). (attention, rien à voir avec le k du k-NN !) 

**Principe**: le jeu de données d'entrainement est divisé en k échantillons (ou "blocs"), habituellement 5. Puis l'un des k échantillons sera utilisé pour valider le modèle tandis que les k - 1 autres échantillons constitueront le jeu de données d'entrainement. Après apprentissage sur le jeu d'entrainement, l'algorithme prédit la classe de chaque échantillon du bloc test, regarde la "vraie" réponse et peut ainsi calculer une performance du modèle. Puis l'opération est répétée en sélectionnant un autre bloc d'échantillons comme test parmi les blocs prédéfinis etc ... A la fin de la procédure on obtient ainsi k notes de performance, une par bloc.

Une fonction de Caret permet de définir cette méthode, _trainControl()_.

A vous : 

* Définir une méthode d'évaluation du modèle grâce à la fonction _trainControl()_, avec la méthode **cv** (cross-validation) et un nombre de blocs à **5** et ranger le résultat dans une variable _fitControl_cv_ qui sera utilisée lors de l'entrainement de l'ensemble de nos modèles (k-NN, random forest et SVM).

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/R_notebooks/machine_learning_introduction.ipynb)

### Normalisation min/max des comptages de gènes et entrainement du modèle k-NN

Il est nécessaire de normaliser les comptages des gènes car le gradient descent, qui permet de trouver les paramètres et optimiser le modèle, n'aime pas les variables avec une trop grande disparité. Nous allons donc faire un normalisation min/max pour obtenir une matrice de comptage avec des valeurs entre 0 et 1.

Dans Caret, la fonction _train()_ permet d'entrainer un modèle et permet aussi de définir la méthode de normalisation avant d'entrainer le modèle. 

A vous : 

* Entrainer un modèle k-NN sur le jeu de données d'entrainement avec la fonction _train()_ de Caret. La formule à utiliser est la suivante : `Organ ~ .`. Utiliser la méthode de normalisation "_range_". Ne pas oublier d'utiliser la variable _fitControl_cv_ définie précédemment pour tester la performance du modèle. Bien préciser qu'il faut entrainer un modèle **knn**. 

Grâce à la méthode de cross-validation, le paramètre k optimal, des k plus proches voisins, a pu être défini par le modèle.

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/R_notebooks/machine_learning_introduction.ipynb)

### Tracer le graphique de la précision du modèle selon les K testés

* Utiliser la fonction _plot()_ sur la variable contenant votre modèle entrainé.

### Utiliser le modèle k-NN entrainé, sur le jeu de données test

* Utiliser la fonction _predict()_ de Caret avec le modèle entrainé et le jeu de données d'entrainement, ce qui permettra de prédire l'organe dont chaque échantillon provient.

* Utiliser la fonction _confusionMatrix()_ pour générer la matrice de confusion, qui permet de tester la précision du modèle. La matrice de confusion matérialise la confrontation entre les classes observées et prédites. Des métriques en sont ensuite déduites. Sur la matrice de confusion, les classes prédites par le modèle sont en ligne et les classes observées en colonne. Le taux de réussite (précision) est donné via la valeur de l'accuracy. L'intervalle de confiance à 95 % est fourni.

Nous allons entrainer d'autre modèles, pour pouvoir comparer leur précision à celle du modèle k-NN.

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/R_notebooks/machine_learning_introduction.ipynb)

### Entrainement du modèle random forest

* Comme pour l'entrainement du modèle k-nn, utiliser la fonction _train()_ avec la méthode **rf** cette fois-ci, pour random forest.

* Utiliser la fonction _predict()_ avec ce modèle entrainé sur le jeu d'entrainement et générer la matrice de confusion avec _confusionMatrix()_.

### Entrainement du modèle SVM

Pour le modèle SVM, un paramètre peut être optimisé, c'est le paramètre **C** : par défaut il est pris à 1 et ne change pas durant l'apprentissage du modèle. C est un paramètre de réglage, également connu sous le nom de Coût, qui détermine les erreurs de classification possibles. Il impose essentiellement une pénalité au modèle pour avoir fait une erreur. Il est possible de calculer la performance d'un modèle SVM pour différentes valeurs de C et de choisir le C optimal celui qui maximise la précision de la validation croisée du modèle. Pour cela, il faudra utiliser une option supplémentaire dans la fonction _train()_ : `tuneGrid = expand.grid(C = seq(0.0001, 2, length = 20))`. Cela permet de tester 20 valeurs de C comprises entre 0.0001 et 2

* Comme pour l'entrainement du modèle k-nn, utiliser la fonction _train()_ avec la méthode **svmLinear** cette fois-ci. 

* Utiliser la fonction _predict()_ avec ce modèle entrainé sur le jeu d'entrainement et générer la matrice de confusion avec _confusionMatrix()_.

## Comparer la précision de plusieurs modèles (k-NN, random forest et SVM)

Pour comparer et tracer graphique la précision de plusieurs modèles, il faut procéder comme suit :

* créer une liste de tous les modèles à comparer, exemple : 

`model_list <- list(knn = fitKNN2, rf = fitRF, svm = fitSVM)`

* Utiliser la fonction _resamples()_ sur cette liste, qui permettra de comparer les résultats de cross validation de chaque modèle.

* Générer un résumé de cette comparaison avec la fonction _summary()_

* Générer un graphique traçant les métriques de précision pour chaque modèle, avec la fonction _bwplot()_





