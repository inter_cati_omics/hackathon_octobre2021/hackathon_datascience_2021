# Data formatting with tidyverse

Le but de cette partie sera de réduire la taille et de formater le jeu de données avec le package **tidyverse**

## Les packages R

Charger les packages **feather**, **stats**, **tidyverse** et **vroom**. S'ils ne sont pas installés, se référer à la partie [Prérequis](book/prerequis.md).

## Lire les métadonnées liées au fichier de comptage

Les métadonnées sont stockées dans un fichier tabulé. Utilisez le package **vroom** pour lire ce fichier. (Voir la fonction _vroom_ du package **vroom**.) [Métadonnées](data/RNA-Seq_datasets_USER_2007.txt). Le fichier contenant les métadonnées est accessible via le chemin relatif _data/RNA-Seq_datasets_USER_2007.txt_ lorsque vous êtes dans le répertoire git.

Lors de la lecture du fichier, définir le type des colonnes : 

* Par défaut, le type doit être de type _factor_. Les colonnes **Paper**, **Short_stage**, **Detailed_stage**, **Treatment_detail**, et **Comment** doivent être de type _character_. La colonne **Read_length** doit être de type _integer_. (Voir l'option _col_types_ de la fonction _vroom()_)

* Définir que les valeurs manquantes doivent être un tiret "-" (voir l'option _na_ de la fonction **vroom()**)

Utiliser un pipe **%>%** pour formater directement le fichier lu : [Cheatsheet dplyr](https://www.rstudio.com/wp-content/uploads/2015/02/data-wrangling-cheatsheet.pdf)

* Remplacer les valeurs de la colonne **Private_public?** par _True_ ou _False_ (vecteur logique). Exemple, **Public** devient **True** et l'autre valeur devient **False**. Voir la fonction _mutate()_ du package **dplyr**.

* Remplacer les valeurs de la colonne **PE_SE** par True ou False (vecteur logique). Exemple, **PE** devient **True** et l'autre valeur devient **False**. Voir la fonction _mutate()_ du package **dplyr**.

* Certaines descriptions d'organes de vigne peuvent être redéfinies :
  
  * Par exemple, il y a une erreur manuelle, avec deux organes dont un est nommé **Inflorescence** et l'autre **inflorescence**. R étant case sensitive, il faut définir une valeur unique pour ce cas, par exemple **Inflorescence**.

  * De même, trois organes peuvent être définis en un seul. Exemple, les organes **Flower**, **Buds** et **Flower buds** peuvent être définis comme un seul organe nommé **Flower**.
  
**Note** : Utiliser la fonction _mutate()_ et la fonction _fct_collapse()_ pour fusionner des niveaux de facteur en un nouveau groupe défini : par exemple, les niveaux **Inflorescence** et **inflorescence** deviennent **Inflorescence**.

* Une colonne contient un caractère spécial qu'on souhaite supprimer. Remplacer le nom de la colonne **Treatment?**	par **Treatment** grâce à la fonction _rename()_ de dplyr.

* Transformer la colonne **Complete_Bioproject** en un vecteur logique, comme précédemment pour la colonne **PE_SE** par exemple.

Après ces diverses étapes, félicitations, le tableau des métadonnées est correctement formaté !

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/R_notebooks/data_formatting_with_tidyverse.ipynb)

## Explorer les métadonnées liées au fichier de comptage

* Utiliser les fonction head() et summary() pour visualiser/explorer le tableau des métadonnées importé.

* Compter le nombre d'échantillons par organe (fonction _count()_ du package dplyr)

## Réduire la matrice de comptage

La matrice de comptage brute contient l'expression de 1991 échantillons pour 41373 gènes, soit 82 millions de valeurs.

Le but va être de réduire dans un premier temps le nombre d'échantillons dans cette matrice pour passer de 1991 échantillons à 715 échantillons. Pour cela on va supprimer aléatoirement des échantillons correspondant aux organes les plus représentés (baies vertes, baies mûres et feuilles).

Afin de sélectionner aléatoirement des échantillons mais de faire en sorte qu’ils soient reproductibles dans le temps ou sur une autre machine, il faut défini set.seed()

`set.seed(12345)`

But : créer un sous ensemble du dataframe contenant les métadonnées, en ne gardant que les organes avec plus de 30 échantillons et en réduisant les organes avec plus de 150 échantillons à 150 échantillons aléatoires.

**Indices** : 

* Chainer les opérations avec l'opérateur pipe **%>%**

* Utiliser la fonction _slice_sample()_ pour sélectionner aléatoirement 150 échantillons / organe

* Utiliser la fonction _filter()_ pour ne garder que les organes avec plus de 30 échantillons

* Utiliser la fonction _ungroup()_ pour revenir au format dataframe originel, mais filtré

* Vérifier que le nouveau dataframe des métadonnées contient bien 715 échantillons (fonction _count()_)

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/R_notebooks/data_formatting_with_tidyverse.ipynb)

## Lecture de la matrice de comptage

* Lire la matrice de comptage ([Matrice](data/RPKN_counts_all_genes.feather)), accessible via le chemin relatif _data/RPKN_counts_all_genes.feather_ lorsque vous être dans le répertoire git. Pour cela, utiliser la fonction _read_feather()_ du package **feather** en sélectionnant directement les 715 échantillons avec l'option _columns_, lors de la lecture.

* Afficher la dimension de cette matrice.

## Filtrer la matrice pour réduire le nombre de gènes (le nombre de lignes)

### Filtrer la matrice pour supprimer les gènes qui ont plus de 3 échantillons avec un comptage à 0

* Différentes manière d'y arriver, possibilité d'utiliser la fonction _rowSums()_ et de supprimer les lignes avec plus de 3 valeurs à 0.

### Filtrer la matrice pour ne garder que les 3000 gènes les plus variables

* Définir la fonction _mostVar()_ contenant le code suivant : 

```
data.var <- apply(data, 1, stats::var)
data[order(data.var, decreasing=TRUE)[1:n],] 
```

Cette fonction prend en entrée la matrice (_data_) et _n_ le nombre de gènes les plus variables à garder.

* Lancer cette fonction sur la matrice pour obtenir une matrice finale de 715 échantillons et 3000 gènes.

* Afficher une vue de la matrice et sa dimension.

Nous venons de passer de 80 millions de valeur à 2 millions de valeurs dans la matrice.

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/R_notebooks/data_formatting_with_tidyverse.ipynb)

## Sources

- Cheatsheet [readr](https://raw.githubusercontent.com/rstudio/cheatsheets/master/data-import.pdf), [tidyr](https://raw.githubusercontent.com/rstudio/cheatsheets/master/tidyr.pdf) et [dplyr](https://raw.githubusercontent.com/rstudio/cheatsheets/master/data-transformation.pdf)
- [dplyr <-> base R](https://dplyr.tidyverse.org/articles/base.html)
