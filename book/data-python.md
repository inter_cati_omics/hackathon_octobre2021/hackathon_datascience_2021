<img src="../img/Pandas_logo.png" width="100"/> 

Dans cette première partie, nous allons apprendre à lire puis manipuler un jeu de données avec Pandas. A l'instar de R, cette librairie permet de générer  à partir d'un jeu de données un ou plusieurs dataframe, et d'offrir une panoplie d'outils performants pour les manipuler. 

## Petite initiation Pandas 

{% pdf title="Slides", src="../slides/Initiation_Pandas.pdf", width="100%", height="550", link=true %}{% endpdf %}

## cheat-Sheet Pandas 

[cheat-sheet](https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf)

## Travaux pratiques 

### Les données 
Les jeux de données, on dispose de 3 fichiers :

1. RPKN_counts_715_samples_3000_most_var_genes.feather 

715 échantillons X 3000 gènes 

<img src="../img/Pandas/RPKM.png" width="500"/> 

2. correspondance_VCostV3_V2_V1_et_annotations.txt

42413 gènes X 14 annotations 

<img src="../img/Pandas/annots.png" width="500"/> 

3. RNA-Seq_datasets_USER_2007.txt

1991 échantillons X 22 descriptions 

<img src="../img/Pandas/metadata.png" width="500"/> 

# Partie 1

Dans cette partie nous allons charger les jeux de données et réaliser quelques requètes pour en extraire de l'information

## Exercices

1. Quelles sont les annotations du gène Vitvi01g01476?

|annotation|Vitvi01g01476|
|---|:---|
|V1_ID | VIT_01s0010g02030|
|V2_ID | Vitvi01g01476 |
|Refseq_ID | LOC100242603 |
|Functional_annotation | Gamma-thionin precursor |
|Symbol | NaN |
|Vitisnet_molecular_network | NaN |
|Best_arabidopsis_match |  AT2G02100 |
|Gene_ontology_GO | GO:0005886;plasma membrane;C|GO:0050832;defens... |
|Plant_ontology_PO | PO:0000230;inflorescence meristem;S|PO:0008019... |
|PFAM_accession | PF00304;Gamma-thionin family |
|SMART_accession | SM00505;Knottins |
|PROSITE_accession | PS00005;Protein kinase C phosphorylation site... | 
|InterPro_accession | IPR008177; IPR008176; IPR003614 |
|UniProtKB_accession | NaN |

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/Python_notebooks/vitis_exploration.ipynb)

2. Quelles sont les librairies (et leurs organes) où le gène Vitvi01g01476 est exprimé à plus de 10000 RPKM?

| sample_name | Organ |
|---|---|
| DRR093291 | stem |
|DRR093292 | stem |
|DRR093298 | stem |
|DRR093299  | stem |
|ERR3163278 | plantlets |
|...|...|                                  
|SRR8979835 | ripening berries |
|SRR8979843 | ripening berries |
|SRR8979851 | ripening berries |
|Vv01_AC07KNACXX_ACAGTG_L003_R1  |     green berries|
|Vv04_AC07KNACXX_ACTTGA_L003_R1   | ripening berries|
Name: Organ, Length: 470, dtype: object

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/Python_notebooks/vitis_exploration.ipynb)

3. Quel est le terme GO le plus courant?


GO:0005515;protein binding;F|GO:0008270;zinc ion binding;F|GO:0046872;metal ion binding;F est associé à 176 gènes


[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/Python_notebooks/vitis_exploration.ipynb)

4. Identifiez les gènes du métabolisme du Glutathione

Pour récupérer les réseaux du métabolisme du Glutathione, il faut utiliser une expression régulière par exemple avec <a href="https://pandas.pydata.org/docs/reference/api/pandas.Series.str.contains.html"> str.contains</a> 


130 gènes :
'Vitvi15g01736', 'Vitvi15g01730', 'Vitvi19g02378', 'Vitvi19g02374',
'Vitvi00g02328', 'Vitvi18g03402', 'Vitvi01g00148', 'Vitvi01g00149',
'Vitvi01g00888', 'Vitvi01g00889',
       ...
 'Vitvi19g02147', 'Vitvi19g02149', 'Vitvi19g02150', 'Vitvi19g02152',
 'Vitvi19g02161', 'Vitvi19g02189', 'Vitvi19g02192', 'Vitvi19g02197',
 'Vitvi19g02257', 'Vitvi19g02302'

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/Python_notebooks/vitis_exploration.ipynb)

5. Donnez la moyenne des expressions des gènes du métabolisme gluthatione par organe

Regroupez vos données à l'aide d'un <a href="https://pandas.pydata.org/docs/reference/groupby.html#groupby"> groupby </a> )
Attention tous les gènes du métabolisme du Glutathione ne sont pas dans le tableau rpkm (il y en a 30)

Voici le résultat pour les 5 premiers

||	Vitvi15g01736|	Vitvi01g00889	|Vitvi02g00296	|Vitvi02g00326 |	Vitvi02g00332 |			
|---|---|---|---|---|---|
|flower |	776.614087 |	2049.480196 |	2766.321501 |	822.953838 |	300.985167 |
green berries |	1480.283216 |	1042.648147 |	3542.406202	| 1557.588693	| 1230.872217|
inflorescence |	679.202939	|2566.846967	|3344.741198	|692.818064	|642.345833|
leaves |	1391.003372	|3908.110297	|2907.444832	|899.802147|	293.770616|
plantlets |	1664.853027	|886.831169	|2530.972846	|1099.908252	|481.763287|
ripening berries |	1173.764942|	1945.323545|	3734.325422|	2164.346038|	96.399208|
shoot |	6645.910454	|1389.328078	|3257.434264	|1419.230622	|1209.477313|
stem |	1318.548068	|327.361217|	3076.188840	|573.070373	|1810.653004|

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/Python_notebooks/vitis_exploration.ipynb)

# Partie 2

Dans cette seconde partie, nous allons créer un nouveau dataframe fusionnant l'ensemble des informations pour les gènes et librairies  dispose du RPKM.

Ce dataframe utilisera 2 indices en lignes et 2 indices en colonne (permettant de séparer le tableau en 2 parties) comme sur la figure 
<img src="../img/Pandas/df_fusionne.png" width="500"/> 

On commence par charger le tableau des valeurs de RPKM. Mais cette fois ci, on lui associe deux indices  pour les lignes  : 'Type' qui vaudra  'Sample'  pour chaque ligne et le nom du sample comme auparavant.

```
complete = pd.read_feather("../../data/RPKN_counts_715_samples_3000_most_var_genes.feather")
complete['Type']= pd.Series("Sample", range(len(complete.index)))
complete.columns.values[0]='name'
complete.set_index(['Type', 'name'],inplace=True)
```

Alors  la commande :

```
complete.loc[("Sample","DRR093296")]
```

retourne : 

|||
|---|---|---|
|Vitvi03g01899 |   1421755.219966 |
|Vitvi19g01871 |      1517.454335 |
|Vitvi13g00384 |      1638.403364|
|Vitvi17g00320 |    206724.234399|
|Vitvi03g01371 |       546.867778|
| ...| ...|      
|Vitvi15g01107 |       884.584613|
|Vitvi03g00315 |       327.084792|
|Vitvi18g00574 |       901.432591|
|Vitvi07g01428 |        134.713184|
|Organ |                   leaves|

Name: (Sample, DRR093296), Length: 3001, dtype: object

On veut aussi séparer les colonnes décrivant le gène de celle décrivant des Metadata (comme l'organe). C'est un peu plus compliqué, il faut fournir comme index une liste comprenant pour chaque colonne le tuple ('Gene', nom du gene) ou ('Metadata', 'Organ')). Pour cela on utilise la méthode <a href="https://pandas.pydata.org/docs/reference/api/pandas.MultiIndex.htmlhighlight=multiindex#pandas.MultiIndex">pd.MultiIndex.from_tuples </a>. 

```
columns_tuples=[]
for item in complete.columns:
    if (item == 'Organ' ):
        columns_tuples.append(("Metadata", item))
    else:
        columns_tuples.append(("Gene", item))
complete.columns = pd.MultiIndex.from_tuples(columns_tuples)
```


## Exercices

6. : A partir de cette structure
    a. donnez le tableau des tous les RPKM pour tous les gènes et pour toutes les librairies 
    
|Sample | Vitvi03g0189 | Vitvi19g01871 |Vitvi13g00384 |Vitvi17g00320 | Vitvi03g01371 |
|---|---| ---|---|---|---|
|Car_BV |1.227769e+05 |24247.583060 |97.874884 | 32028.068181 |9026.134581 |
|DRR093291 |1.096689e+06 |1098.210345 |688.514747 |39740.189611 |881.171545 |
|DRR093292 |1.032762e+06 |89.283837 |668.595309 |47827.220078 |663.813569 |
|DRR093293 |8.392859e+05 |118.783154 |743.826159 |56517.534962 |434.559528 |
|DRR093296 |1.421755e+06 |1517.454335 |1638.403364 |206724.234399 |546.867778 |

    b. renvoyez le tableau des organes par échantillons
    
|Type |    name |                |
|---|---| ---|
|Sample|  Car_BV |                              green berries |
||        DRR093291 |                                     stem |
||        DRR093292 |                                    stem |
||        DRR093293 |                                    stem |
||        DRR093296 |                                  leaves |
|| ...| ...|                                                       
|| SRR8979835 |ripening berries|
||        SRR8979843   |                     ripening berries |
||        SRR8979851   |                     ripening berries |
||        Vv01_AC07KNACXX_ACAGTG_L003_R1 |       green berries |
||        Vv04_AC07KNACXX_ACTTGA_L003_R1 |  ripening berries |
    
    c. Renvoyez les expressions de chaque gène dans l'échantillon 'Car_BV'
|---|---|
|Vitvi03g01899 |     122777 |
|Vitvi19g01871  |  24247.6 |
|Vitvi13g00384  |   97.8749 |
|Vitvi17g00320  |  32028.1 |
|Vitvi03g01371   | 9026.13 |
 | ... |---|   
|Vitvi04g01536 |   447.381 |
|Vitvi15g01107 |   184.401 |
|Vi03g00315 |    128.127 |
|Vitvi18g00574  |   1279.61|
|Vitvi07g01428 |   1113.52|

    d. Renvoyez l'expression du gène 'Vitvi18g00574' dans la librairie 'Car_BV' 

1279.6125463524982

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/Python_notebooks/prepare_data.ipynb)

7. créez le tableau des annotations (correspondance_VCostV3_V2_V1_et_annotations.txt), avec les gènes en colonne et les annotations en ligne, et un index double pour les colonnes ('Gene' et le gene_id) et un index double pour les lignes ('Type' avec pour valeur 'Annotation' pour chaque annnotation,  et le nom de l'annotation). Pour générer les tuples des doubles index, vous pourrez utiliser la méthode <a href=https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.MultiIndex.from_product.html">MultiIndex.from_product</a>

<img src="../img/Pandas/df_fusionne.png" width="500"/>

[Solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/Python_notebooks/prepare_data.ipynb)

8. a. Ajoutez les lignes du tableau annot au tableau des RPKM en faisant attention à ne conserver que les gènes ayant une valeur RPKM
   b. A partir de ce tableau affichez les annotations du gène Vitvi02g01780

|name | |
|---|---|
|V3_ID |Vitvi02g01780 |
|V1_ID |VIT_02s0234g00130 |
|V2_ID | Vitvi00g00236 |
|Refseq_ID |LOC100254640 |
|Functional_annotation |Ethylene responsive element binding factor 1 |
|Symbol | ERF1_9 |
|Vitisnet_molecular_network |   vv30008Ethylene_signaling vv30011Jasmonate_sig...|
|Best_arabidopsis_match | NaN|
|Gene_ontology_GO | GO:0006350;transcription;P|GO:0003677;DNA bind... |
|Plant_ontology_PO | PO:0000230;inflorescence meristem;S|PO:0008019... |
|PFAM_accession | PF00847;AP2 domain |
|SMART_accession | SM00380;DNA-binding domain in plant proteins s... |
|PROSITE_accession | PS00004;cAMP- and cGMP-dependent protein kinas... |
|InterPro_accession | NaN |
|UniProtKB_accession | NaN |
Name: (Gene, Vitvi02g01780), dtype: object

9. Créez le tableau metadata en ajoutant un double index en ligne ("Tyoe" avec pour valeur 'Sample', et les nom des sample) et en colonne ("Metadata" et le nom des metadonnées) ajouter ces colonnes au fichier précédemment créé, en ne conservant que les échantillons du tableau RPKM, et en  supprimant la colonne Organ (déjà présente dans le tableau précédent). 
