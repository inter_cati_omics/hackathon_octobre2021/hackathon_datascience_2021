<img src="../img/Pandas/seaborn_logo.svg" width="100"/> 

# Introduction 

Dans cette partie nous allons utiliser principalement la librairie <a href="https://seaborn.pydata.org/Seaborn"> Seaborn</a> pour produire plusieurs types de graphiques à partir du jeu de données vitis obtenus à l'aide du notebook [prepare_data.ipynb](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/Python_notebooks/prepare_data.ipynb) et sauvegardé en pickle dans le répertoire data.
    
Il est aisé de créer très rapidement des graphiques à l'aide commandes Seaborn très simples. Souvent, la réelle difficulté réside en la transformation du jeu de données afin que Seaborn puisse afficher les données désirées. Les données acceptées par Seaborn sont de 2 types (cf <a href="https://seaborn.pydata.org/tutorial/data_structure.html">), wide ou long</a>. Le format Wide est souvent plus simple et plus léger à utilisé mais selon le nombre de variables et leur type, il est régulier de transformer sa table en format long à l'aide de la commande <a href="https://pandas.pydata.org/docs/reference/api/pandas.melt.html">melt</a> (l'inverse est aussi possible avec la commande  <a href="https://pandas.pydata.org/docs/reference/api/pandas.pivot.html">pivot</a>).

Une fois, que l'un des premiers jet d'un graphique est produit, et qu'on est satisfait de la représentation des données, il est souvent utile de modifier/configurer beaucoup d'éléments esthétiques. 5 styles sont proposés (darkgrid, whitegrid, dark, white, et ticks). Ces styles peuvent être appliqués avec la commande set_style("nom_du_style"), comme suit :

```
import seaborn as sn
sn.set_style("dark")
```
On peut faire varier les paramètres d'un style. L'ensemble des paramètres d'un  style est accessible à l'aide de la commande :

```
sns.axes_style()
```
Il est alors possible de modifier certaines valeurs avec la commande set_style comme dans l'exemple ci-dessous: 
```
sns.set_style("darkgrid", {"axes.facecolor": ".9"})
sns.axes_style())
```


Par ailleurs, les dimensions relatives des éléments d'un graphique (labels, légendes, taille des points et des courbes, ...) dépendent du format du document dans lequel le graphique doit être utilisé. Seaborn propose 4 thèmes (paper, notebook, talk et  poster), qui peuvent être appliqués comme suit : 

```
import seaborn as sn
sn.set_context("notebook")
```
Comme pour les styles, on peut aussi faire varier précisément les paramètres d'un contextes listables avec la commande sn.plotting_context(), en passant le ou les paramètres à modifier sous la forme d'un dictionnaire comme dans la commande ci-dessous :

```
sns.set_context("notebook", rc={'xtick.labelsize': "small"})
sns.plotting_context()
```

La palette  de couleurs utisées dans un graphique peut aussi être modifiée grace à la commande set_palette(). Seaborn propose plusieurs <a href="https://gist.github.com/mwaskom/b35f6ebc2d4b340b4f64a4e28e778486#file-palettes-png">palette de couleurs </a> (deep, muted, pastel, bright, dark, and colorblind). Mais les palettes de <a href="https://colorbrewer2.org/"> Color Brewer </a> peuvent aussi être uitlisées avec les mêmes commandes. Pour visualiser les couleurs d'une palette, vous pouvez lancer la commande color_palette("palette"). 

Seaborn permet de créer beaucoup de style de graphiques, des nuages de points, des courbes, des histogrammes, des boxplots. Ces graphiques peuvent aussi être organisé sous la forme d'un grille (grid, facetgrid). Pour avoir, plus de détails sur la génération de ces graphiques, vous pouvez vous aider de l'<a href="https://seaborn.pydata.org/api.html">API</a> ou vous inspirez du large panel d'exemple disponible <a href="https://seaborn.pydata.org/examples/index.html">en ligne</a>.


# Réalisation de graphique sur le jeu de données Vitis

Dans le cadre de l'hackathon, nous allons réaliser plusieurs graphiques. L'ensemble des scripts est disponible dans le notebook [vitis_plot.ipynb](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/Python_notebooks/vitis_plot.ipynb).

seaborn notebook [solution](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/Python_notebooks/vitis_plot.ipynb)

## Exemple 1

<img src="../img/Pandas/graphes/exemple1.png" width="300"/> 

Dans cet exemple on affiche un nuage de points incluant les valeurs d'expression de 2 gènes Vitvi13g00384 et Vitvi17g00320 dans l'ensemble des échantillons.

## Exemple 2

<img src="../img/Pandas/graphes/exemple2.png" width="300"/> 

On reprend l'exemple précédent, mais on ajoute par des couleurs l'organe correspondant aux échantillons 

## Exemple 3

<img src="../img/Pandas/graphes/exemple3.png" width="300"/> 

On représente une courbe de densité représentant toutes les valeurs d'expression du gène Vitvi13g00384. Attention, il faut préalablement transformer les valeurs en float64 (avec la méthode astype("float64"))

## Exemple 4

<img src="../img/Pandas/graphes/exemple4.png" width="300"/>

Dans cet exemple, on représente de nouveau un nuage de point mais cette fois ci en représentant les valeurs de l'ensemble des gènes de échantillons DRR093292 et DRR093293. 

## Exemple 5

<img src="../img/Pandas/graphes/exemple5.png" width="300"/>

On observe qu'il y a un point aberrant. On passe en log les 2 axes afin d'avoir une meilleure visualisation.

## Exemple 6

<img src="../img/Pandas/graphes/exemple6.png" width="300"/>

Pour la suite des opérations, on préfère retirer cet élément. Trouvez un moyen d'identifier le gène correspondant (Vitvi03g01899), d'afficher ses annotations,et de le supprimer du jeu de données vitis.

## Exemple 7

<img src="../img/Pandas/graphes/exemple7.png" width="300"/>

Pour réaliser ce graphe, on normalise les données avec la fonction <a href="https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.normalize.html">normalize</a> de  librairie sklearn

## Exemple 8

<img src="../img/Pandas/graphes/exemple8.png" width="300"/>

On réalise un boxplot représentant l'ensemble de l'expression des gènes par organe. Pour cela, il est utile de transformer la table vitis en forme longue à l'aide de la fonction <a href="https://pandas.pydata.org/docs/reference/api/pandas.melt.html">melt</a>

## Exemple 9 

<img src="../img/Pandas/graphes/exemple9.png" width="300"/>

L'exemple précédent étant peu lisible, on réalise également un boxplot mais pour les valeurs d'expression d'un gène unique.

## Exemple 10

<img src="../img/Pandas/graphes/exemple10.png" width="300"/>

On réalise également un boxplot mais en prenant en compte les valeurs d'expression des gènes impliqués dans la voie de biosynthèse  des phenylanaline, tyrosine et tryptophane  ("vv10400Phenylalanine_tyrosine_and_tryptophan_biosynthesis" de Vitisnet).

## Exemple 11

<img src="../img/Pandas/graphes/exemple11.png" width="300"/>

Plus compliqué! On analyse les valeurs d'expression des gènes es échantillons à plusieurs stades de développement de la plante ('BBCH 55', 'BBCH 57', 'BBCH 75', 'BBCH 77',  'BBCH 78',  'BBCH 79', 'BBCH 81', 'BBCH 83', 'BBCH 85', 'BBCH 89') indiqués dans la valeur 'Short_stage' de vitis, pour les gènes impliqués dans les voies 'vv10941Flavonoid_biosynthesis', 'vv10860Porphyrin_and_chlorophyll_metabolism','vv10480Glutathione_metabolism', 'vv10100Biosynthesis_of_steroids', 'vv10940Phenylpropanoid_biosynthesis', et 'vv10500Starch_and_sucrose_metabolism'.

## Exemple 12

<img src="../img/Pandas/graphes/exemple12.png" width="300"/>

On réalise maintenant une heatmap à partir d'une matrice de corrélation (réalisé avec la méthode <a href="https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.corr.html">corr</a> de Pandas) en prenant toutes les valeurs d'expression des gènes pour les échantillons de croissance de la plante. Attention pour réaliser la corrélation, il faut une nouvelle fois convertir les données d'ex
pression en float64.

## Exemple 13

<img src="../img/Pandas/graphes/exemple13.png" width="300"/>

Encore, une heatmap, mais cette fois-ci on réalise la corrélation entre les stades de croissances. On se base alors sur la moyenne d'expression des gènes dans ces échantillons (utilisation de la méthode <a href="https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.groupby.html?highlight=groupby#pandas.DataFrame.groupby">groupby </a> de Pandas) 

## Exemple 14

<img src="../img/Pandas/graphes/exemple14.png" width="300"/>

Même chose que précédemment, mais cette fois-ci on calcule les corrélations entre les organes basée sur la  valeur moyenne d'expression des gènes dans ces échantillons.

## Exemple 15

<img src="../img/Pandas/graphes/exemple15a.png" width="300"/>
<img src="../img/Pandas/graphes/exemple15b.png" width="300"/>
<img src="../img/Pandas/graphes/exemple15c.png" width="300"/>

On réalise une ACP  en 3 axes avec la fonction <a href="https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html">PCA</a> de sklearn, en conservant la variance expliquée par les 3 axes (obtenus par l'attribut explained_variance_ratio_ de l'objet PCA).

Puis on affiche en nuage de points coloré par organe en se basant sur les coordonnées des points sur les axes.

## Exemple 16

<img src="../img/Pandas/graphes/exemple16.png" width="300"/>

A partir du même jeu de données on visualise les 3 variables (coordonnées sur chaque axe) dans un  graphe unique 2 à 2 en utilisant pairplot

## Exemple 17

<img src="../img/Pandas/graphes/exemple17.png" width="300"/>

Cet exemple est réalisé en utilisant la méthode <a href="https://matplotlib.org/stable/api/_as_gen/mpl_toolkits.mplot3d.axes3d.Axes3D.html">Axes3D</a> de matplotlib.  