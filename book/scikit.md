# Introduction to Machine Learning with Python

Introducing slides:

{% pdf title="Slides", src="../slides/Hackathon_inter-CATI_Datascience_ML.pdf", width="100%", height="550", link=true %}{% endpdf %}


Context / Biological problem:

Based on a expression matrix file of several hundred RNA-Seq samples from different grapevine organs, we will try to train a model to predict the organ of a non-identified sample. Processes used in this tutorial were mainly adapted from [saclay-datascience-workshop](https://github.com/paris-saclay-cds/data-science-workshop-2019) and the [scikit-learn website](https://scikit-learn.org/).


Here find a solution to the different steps: [ML in Python](https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021/-/blob/main/notebook/Python_notebooks/machine-learning.ipynb)


## 1) Prepare data for ML

Prepare your data ! Read the data from the feather file (in data/RPKN_counts_715_samples_3000_most_var_genes.feather). Extract numerical values and classes to predict, then use the `from sklearn.model_selection import train_test_split` method to prepare your training and test datasets. Use the 3 most representative organs, other will be used later to test the specificity of our model.

## 2) Classification with Linear Model ([Logistic Regression](https://scikit-learn.org/stable/modules/linear_model.html#logistic-regression))

### a) In an easy and fast way: Train, Predict and Evaluate

Use a logistic regression as classifier, train the model and predict with the different sets.

### b) Compare to a dummy classification ([DummyClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.dummy.DummyClassifier.html#sklearn.dummy.DummyClassifier))

Use a  dummy classifier `from sklearn.dummy import DummyClassifier` and compare the score with your model.

### c) Cross-validation: repeat and confirm

Several cross-validation methods are available, see [Model Validation](https://scikit-learn.org/stable/modules/classes.html#model-validation)

Use a cross validation of your model, running several time the training process and compare the different scores.

### d) Scaling data

Scaling data help the model to fit faster, avoiding large discrepancies between variables.

Use the preprocessing tools to scale your data `from sklearn.preprocessing import StandardScaler`


### d) Scaling, Model, ... combine in a pipeline

Include all previous steps in a pipeline: `from sklearn.pipeline import make_pipeline`

### e) Specificity, test on trained/untrained classes

Extract the unused organs and run the model with. Evaluate the specificity of our model.

### f) Extra 😰
Limit the number of genes used to classify the 3 most representative organs with at minimum score of 0.9 for your model. Extact the genes and find associate annotations. Try to build an iterative process to reduce the number of genes used at each time up to the desired threshold for the score of the model. How many genes are required ? 
Plot their expression values in the different samples/organs.


## 3) Classification with Support Vector Machines (SVM)

Try another model with SVM and plot score with a cross_validation_curve.

## 4) Misc

### a) Hyperparameters

The process of learning a predictive model is driven by a set of internal parameters and a set of training data. These internal parameters are called hyper-parameters and are specific for each family of models. In addition, a specific set of parameters are optimal for a specific dataset and thus they need to be optimized. See more on [Tune Hyperparameters in practice](https://github.com/paris-saclay-cds/data-science-workshop-2019/blob/master/Day_2_Machine_Learning_Python/04_basic_parameters_tuning.ipynb) and [Tune Hyperparemeters](https://machinelearningmastery.com/hyperparameters-for-classification-machine-learning-algorithms/).

Try to change your hyperparameters and visualize the results. 
