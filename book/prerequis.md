# Prérequis

## Machine virtuelle

Une machine virtuelle (Ubuntu) fonctionnant sous virtualbox est disponible ici: [vm_datascience](https://bioinfo.bioger.inrae.fr/portal/data-browser/misc/hackathon_cati_2021/DataScience.ova) (environ 11Go)

info de connection:
* login: datascience
* password: datascience

Le code de la session issu du projet gitlab (https://forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/hackathon_datascience_2021) est disponible sous /home/datascience/Hackathon/hackathon_datascience_2021.

Pour lancer le jupyterlab:
```
# source the python virtualenv
source /home/datascience/venvs/pydata/bin/activate
# launch jupyterlab
jupyter lab
``` 

## Installation manuelle

Si vous voulez utiliser votre propre machine en installant les outisl/packages nécessaires à l'atelier.

### Installation Linux sous Windows 10

C'est possible aussi sous Windows 10. Mais on vous conseille d'installer une version de linux (par exemple debian dans WSL (le linux de windows). Pour cela :


En tant qu'administrateur
Activer la fonctionnalité "Sous système Windows pour Linux" (taper fonctionnalités dans le menu rechercher puis "Activer ou Desactiver les fonctionnalités windows")

Installer Debian depuis le windows store : https://aka.ms/wslstore

Lancer bash depuis le menu Installation Linux sur windows 10
Créer un nouvel utilisateur (adduser -m -p password login )

### Python

Voici une procedure (sous Ubuntu) pour créer un environement virtuel Python et installer les packages nécessaires à l'atelier en Python.

Valider la présence de Python3:
```
# python
which python3
/usr/bin/python3
python3 -V
Python 3.8.10
```
Créer votre virtualenv en installant les dependances necessaires:
```
# virtualenv
sudo apt install python3-pip
sudo apt-get install python3-virtualenv
mkdir /home/datascience/venvs
virtualenv -p /usr/bin/python3 /home/datascience/venvs/pydata
source /home/datascience/venvs/pydata/bin/activate
```
Installer jupyter lab:
```
# install jupyterlab 
pip install jupyterlab
# launch jupyterlab
jupyter lab 
```
Ajouter le plugin git 
```
sudo apt-get install Node.js
sudo apt-get install git
sudo pip3 install --upgrade jupyterlab-git
sudo jupyter lab build
```

Récuperer le code du hackathon (en ligne mais possible aussi dans jupyter-lab):
```
# get code hackathon
mkdir /home/datascience/Hackathon
cd /home/datascience/Hackathon
# create personal access token forgemia (avoid keys if shared vm) or add your ssh keys
git clone https://forgemia.inra.fr/inter_cati_omics/hackathon_datascience_2021.git
```
Installer les modules necessaires dans le virtualenv:
```

# libs
sudo pip3 install numpy pandas matplotlib plotly bokeh seaborn scipy  scikit-learn gseapy
# install python module
pip install pandas
pip install numpy
pip install matplotlib plotly bokeh seaborn
pip install feather-format
pip install scipy  scikit-learn gseapy
pip install pandas-profiling
pip install ipywidgets # for display pandas_profiling
pip install jupyter-client

jupyter nbextension enable --py widgetsnbextension
or
jupyter nbextension enable --py widgetsnbextension --sys-prefix
```
Lancer jupyter lab:
```
jupyter lab
```

### R

Deux possibilités : travailler sous JupyterLab **ou** travailler sous RStudio

#### Sous JupyterLab 

Pour ubuntu, suivre la procédure décrite dans la partie Python pour installer JupyterLab, puis installer le IRkernel avec les commandes suivantes : 

**Installer R sur Ubuntu :**

```
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/'
sudo apt install r-base
# needs to install devtools
sudo apt install libcurl4-openssl-dev libssl-dev libxml2-dev
```


**Charger l'environnement pydata et lancer R**

```
source /home/datascience/venvs/pydata/bin/activate
# need to change size of plots in jupyter lab R kernel
wget https://github.com/jgm/pandoc/releases/download/2.14.2/pandoc-2.14.2-linux-amd64.tar.gz
sudo tar xvzf pandoc-2.14.2-linux-amd64.tar.gz --strip-components 1 -C /usr/local
sudo R
```

**Installer IRkernel : **

```
install.packages("devtools")
devtools::install_github("IRkernel/IRkernel")
IRkernel::installspec()
```

**Installer les packages R**

`install.packages(c("tidyverse", "shiny", "golem", "feather", "caret", "e1071", "randomForest", "devtools", "rsconnect", "plotly", "ggplot2"))`

Sinon, il est possible d'installer la suite Anaconda directement sur votre ordinateur (Linux, Windows, macOS), elle contient JupyterLab et les kernels R et Python directement : [Anaconda](https://www.anaconda.com/products/individual). C'est juste plus lourd car plein d'outils autres à côté.

#### Sous Rstudio

Pour le bon déroulement du TP, merci de suivre les installations suivantes :

* [R](https://rstudio.com/products/rstudio/download/#download)
* Pour les utilisateurs de Windows, [RTools](http://jtleek.com/modules/01_DataScientistToolbox/02_10_rtools/#1)
* [RStudio](https://rstudio.com/products/rstudio/download/#download)
* Les packages `install.packages(c("tidyverse", "shiny", "golem", "feather", "caret", "e1071", "randomForest", "devtools", "rsconnect", "plotly", "ggplot2"))`
