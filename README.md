# Hackathon DataScience 2021

![python](img/python.png)
![R](img/Rlogo.png)

> lien vers le gitbook [gitlab pages](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/hackathon_datascience_2021/)

{% pdf title="Slides de la demi-journée préparatoire", src="slides/Intro_DataSciences_2021-09-30.pdf", width="100%", height="550", link=true %}{% endpdf %}

## Programme

### mardi 05/10

#### après-midi:

* Prise en main de l'interface de développement (Jupyter lab ou Rstudio)

* Import, préparationet  manipulation du jeu de données. 
  * [![R](img/Rlogo_small.png)](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/hackathon_datascience_2021/book/data-r.html) / [![python](img/python_small.png)](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/hackathon_datascience_2021/book/data-python.html)

### mercredi 06/10

#### matin:

* Graphiques simples / dynamiques avec ggplot2/plotly (R) ou matplotlib/seaborn (Python).
  * [![R](img/Rlogo_small.png)](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/hackathon_datascience_2021/book/ggplot2.html) / [![python](img/python_small.png)](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/hackathon_datascience_2021/book/pandas_visu.html) 

#### après-midi:

* Création d'une application web interactive avec Shiny (R) ou Bokeh (Python). 
  * [![R](img/Rlogo_small.png)](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/hackathon_datascience_2021/book/shiny.html) / [![python](img/python_small.png)](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/hackathon_datascience_2021/book/bokeh.html) 

* Introduction à l'utilisation de bibliothèques de Machine Learning. Premiers pas avec Caret en R et Scikit-learn en Python. 
  * [![R](img/Rlogo_small.png)](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/hackathon_datascience_2021/book/caret.html) / [![python](img/python_small.png)](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/hackathon_datascience_2021/book/scikit.html)

### jeudi 07/10

#### matin:

* Rendu des différents ateliers / discussion participants / comparaisons R vs Python / Perspectives

## Références 

* [R4DS](https://r4ds.had.co.nz)
* [R Cheatsheet](https://www.rstudio.com/resources/cheatsheets/)
* [Python for Data Analysis](https://bedford-computing.co.uk/learning/wp-content/uploads/2015/10/Python-for-Data-Analysis.pdf)
* [Python Cheatsheet](https://www.datacamp.com/community/data-science-cheatsheets)

## Animateurs / Contacts

* [Amandine Velt](mailto:amandine.velt@inrae.fr)
* [Cédric Midoux](mailto:cedric.midoux@inrae.fr)
* [Fabrice Legeai](mailto:fabrice.legeai@inrae.fr)
* [Nicolas Lapalu](mailto:nicolas.lapalu@inrae.fr)
