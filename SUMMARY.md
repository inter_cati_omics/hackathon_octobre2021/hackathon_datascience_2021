# Summary

* [Prérequis](book/prerequis.md)
* Manipulation de données
  * [Python](book/data-python.md)
  * [R](book/data-r.md)
* Visualisation
  * [Python](book/matplotlib.md)
  * [R](book/ggplot2.md)
* Applications
  * [Python](book/bokeh.md)
  * [R](book/shiny.md)
* Machine Learning
  * [Python](book/scikit.md)
  * [R](book/caret.md)
